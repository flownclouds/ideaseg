## ideaseg 

`ideaseg` 是一个基于最新的 [HanLP](https://github.com/hankcs/HanLP/tree/1.x) 自然语言处理工具包实现的中文分词器，
包含了最新的模型数据，同时移除了 HanLP 所包含的非商业友好许可的 [NeuralNetworkParser](https://github.com/hankcs/HanLP/issues/644) 相关代码和数据。

`HanLP` 相比其他诸如 `IK`、`jcseg` 等分词器而言，在分词的准确率上有巨大的提升，但速度上有所牺牲。
通过对 `HanLP` 进行优化配置，`ideaseg` 在准确度和分词速度上取得了最佳的平衡。

与其他基于 `HanLP` 的插件相比，`ideaseg` 同步了最新 `HanLP` 的代码和数据，去除了无法商用的相关内容；实现了自动配置；
包含了模型数据，无需自行下载，使用简单方便。

`ideaseg` 提供三个模块包括：

1. `core` ~ 核心分词器模块
2. `elasticsearch` ~ ElasticSearch 的 ideaseg 分词插件 (最高支持 7.10.2 版本)
3. `opensearch` ~ OpenSearch 的 ideaseg 分词插件 (默认版本 2.4.1)

**关于 `ElasticSearch` 的版本说明，由于从 7.11.1 版本开始 Elastic 修改 ES 的许可证，同时修改了插件的权限策略，
不再允许插件对文件进行读写。由于 `HanLP` 本身的模型数据很大，为了提升速度其处理机制需要在插件的数据目录下生成一些相当于缓存的文件。
因此，如果你使用的是 `ElasticSearch` 请尽量用 7.10.2 或者以下的版本，推荐使用 `OpenSearch` 。**

此外 `data` 包含 `HanLP` 的模型数据。

由于 `ElasticSearch` 的插件机制严格绑定引擎本身的版本，而且版本众多，因此本项目不提供预编译的二进制版本，你需要自行下载源码进行构建。

### 构建

以下是插件的构建过程，在开始之前请先安装好 `git`、`java`、`maven` 等相关工具。

首先确定你的 `ElasticSearch` 或者 `OpenSearch` 的具体版本，假设你使用的是 `ElasticSearch` 7.10.2 版本，
请使用文本编辑器打开 `ideaseg/elasticsearch/pom.xml` 文件，修改 `elasticsearch.version` 对应的值为 `7.10.2` 
（如果是 `OpenSearch` 请修改 `opensearch/pom.xml`）。

保存文件并打开命令行窗口，执行如下命令开始构建：

```shell
$ git clone https://gitee.com/indexea/ideaseg
$ cd ideaseg
$ mvn install
```

构建完成后，将在 `elasticsearch/target` 和 `opensearch/target` 各生成两个插件文件为 `ideaseg.zip` 。

### 安装

构建完成后，我们可以利用 `ElasticSearch` 或 `OpenSearch` 提供的插件管理工具进行安装。

`ElasticSearch` 对应的插件管理工具为 `<elasticsearch>/bin/elasticsearch-plugin` ，
而 `OpenSearch` 对应的管理工具为 `<opensearch>/bin/opensearch-plugin`。
其中 `<elasticsearch>` 和 `<opensearch>` 为两个服务安装后所在的目录。

#### ElasticSearch 安装 ideaseg 插件

```shell
$ bin/elasticsearch-plugin install file:///<ideaseg>/elasticsearch/target/ideaseg.zip
```

#### OpenSearch 安装 ideaseg 插件

```shell
$ bin/opensearch-plugin install file:///<ideaseg>/opensearch/target/ideaseg.zip
```

其中 `<ideaseg>` 为 `ideaseg` 源码所在的路径。要特别注意到是路径前必须有 `file://` 。

安装过程会询问插件所需的权限，回车确认即可完成安装，安装完毕需要重启服务才能让插件生效。

接下来你可以使用分词测试工具来对插件进行测试，如下所示：

```
POST _analyze
{
  "analyzer": "ideaseg",
  "text":     "你好，我用的是 ideaseg 分词插件。"
}
```

关于分词测试的详情请参考 [ElasticSearch 官方文档](https://www.elastic.co/guide/en/elasticsearch/reference/7.10/test-analyzer.html)。

### 反馈问题

如果你在使用 `ideaseg` 过程中有任何问题，请通过 [Issues](https://gitee.com/indexea/ideaseg/issues) 提出。

### 特别感谢

https://github.com/KennFalcon/elasticsearch-analysis-hanlp