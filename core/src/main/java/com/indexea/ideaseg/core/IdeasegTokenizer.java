package com.indexea.ideaseg.core;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.corpus.tag.Nature;
import com.hankcs.hanlp.seg.Other.DoubleArrayTrieSegment;
import com.hankcs.hanlp.seg.Segment;
import com.hankcs.hanlp.seg.common.Term;
import com.hankcs.hanlp.utility.TextUtility;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Ideaseg 分词器
 */
public final class IdeasegTokenizer extends Tokenizer {

    private final static List<Nature> ignoreNatures = Arrays.asList(Nature.w, Nature.p, Nature.uj, Nature.ude1);
    private final static List<String> reveredWords = Arrays.asList();//"++"

    /**
     * 当前词
     */
    private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
    /**
     * 偏移量
     */
    private final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
    /**
     * 距离
     */
    private final PositionIncrementAttribute positionAttr = addAttribute(PositionIncrementAttribute.class);
    /**
     * 词性
     */
    private final TypeAttribute typeAtt = addAttribute(TypeAttribute.class);

    /**
     * 单文档当前所在的总offset，当reset（切换multi-value fields中的value）的时候不清零，在end（切换field）时清零
     */
    private int totalOffset = 0;

    private Segment originSegment;
    private SegmentWrapper segmenter;
    private String indexName;

    /**
     * 创建分词器
     *
     * @param indexName 索引名称
     * @param mode      是否启用极速分词
     */
    public IdeasegTokenizer(String indexName, TokenizerMode mode) {
        this.indexName = indexName;
        originSegment = mode == TokenizerMode.speed ? new DoubleArrayTrieSegment() ://极速分词
                HanLP.newSegment() //标准分词
                        .enableIndexMode(true)
                        .enableMultithreading(true)
                        .enableOrganizationRecognize(true)
                        .enableJapaneseNameRecognize(true)
                        .enableNumberQuantifierRecognize(true);
        this.segmenter = new SegmentWrapper(this.input, originSegment);
    }

    @Override
    public boolean incrementToken() {
        clearAttributes();
        int position = 0;
        Term term;
        boolean unIncreased = true;
        do {
            term = segmenter.next();
            if (term == null) {
                totalOffset += segmenter.offset;
                return false;
            }
            if (TextUtility.isBlank(term.word)) {
                totalOffset += term.length();
                continue;
            }

            if (ignoreNatures.contains(term.nature)) {
                if (reveredWords.contains(term.word)) {
                    position++;
                    unIncreased = false;
                } else
                    totalOffset += term.length();
            } else {
                position++;
                unIncreased = false;
            }
        }
        while (unIncreased);

        positionAttr.setPositionIncrement(position);
        termAtt.setEmpty().append(term.word);
        offsetAtt.setOffset(correctOffset(term.offset), correctOffset(term.offset + term.word.length()));
        typeAtt.setType(term.nature == null ? "null" : term.nature.toString());
        totalOffset += term.length();
        return true;
    }

    @Override
    public void end() throws IOException {
        super.end();
        offsetAtt.setOffset(totalOffset, totalOffset);
        totalOffset = 0;
    }

    /**
     * 必须重载的方法，否则在批量索引文件时将会导致文件索引失败
     */
    @Override
    public void reset() throws IOException {
        super.reset();
        segmenter.reset(new BufferedReader(this.input));
    }

}
