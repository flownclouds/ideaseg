package com.indexea.ideaseg.core;

/**
 * 词条
 */
public class Token implements Comparable<Token> {

    public String word; //词条文本
    public int startOffset; //开始位置
    public int endOffset; //结束位置

    public Token(String word, int start, int end) {
        this.word = word;
        this.startOffset = start;
        this.endOffset = end;
    }

    public Token(char ch, int start, int end) {
        this(String.valueOf(ch), start, end);
    }

    @Override
    public int compareTo(Token o) {
        return startOffset - o.startOffset;
    }

    @Override
    public String toString() {
        return "{ " +
                "word='" + word + '\'' +
                ", startOffset=" + startOffset +
                ", endOffset=" + endOffset +
                " }";
    }
}
