package com.indexea.ideaseg.core;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;

/**
 * Ideaseg 分词器
 */
public class IdeasegAnalyzer extends Analyzer {

    private String indexName;
    private TokenizerMode mode;

    public IdeasegAnalyzer(String indexName, TokenizerMode mode) {
        this.indexName = indexName;
        this.mode = mode;
    }

    @Override
    protected TokenStreamComponents createComponents(String s) {
        return new TokenStreamComponents(new IdeasegTokenizer(indexName, mode));
    }

    @Override
    protected TokenStream normalize(String fieldName, TokenStream in) {
        return new LowerCaseFilter(in);
    }

}
