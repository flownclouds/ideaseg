package com.indexea.ideaseg.core;

import com.hankcs.hanlp.corpus.io.IIOAdapter;

import java.io.*;

/**
 * 该类在 hanle.properties 的 IOAdapter 配置使用
 * 主要目的是实现动态设置 HanLP 的数据目录
 * IOAdapter = com.indexea.ideaseg.core.IdeasegIOAdapter
 */
public class IdeasegIOAdapter implements IIOAdapter {

    public static String BASE_PATH = "";

    @Override
    public InputStream open(String path) throws IOException {
        return new FileInputStream(resolveFile(path));
    }

    @Override
    public OutputStream create(String path) throws IOException {
        return new FileOutputStream(resolveFile(path));
    }

    private static String resolveFile(String path) {
        return BASE_PATH + path;
    }
}
