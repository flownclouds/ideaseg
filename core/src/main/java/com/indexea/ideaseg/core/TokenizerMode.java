package com.indexea.ideaseg.core;

/**
 * ideaseg 支持的几种分词模式
 */
public enum TokenizerMode {
    standard,
    speed,
}
