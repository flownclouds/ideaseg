package com.indexea.ideaseg.core;

public interface PluginBase {

    /**
     * 插件名称
     */
    String PLUGIN_NAME = "ideaseg";

    /**
     * 标准分词名称
     */
    String MODE_STANDARD = "ideaseg";

    /**
     * 极速分词名称
     */
    String MODE_SPEED = "ideaseg_speed";
}
