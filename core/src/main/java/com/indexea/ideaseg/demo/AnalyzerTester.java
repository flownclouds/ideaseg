package com.indexea.ideaseg.demo;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.model.crf.CRFSegmenter;
import com.hankcs.hanlp.model.perceptron.PerceptronLexicalAnalyzer;
import com.hankcs.hanlp.model.perceptron.PerceptronSegmenter;
import com.hankcs.hanlp.tokenizer.SpeedTokenizer;
import com.indexea.ideaseg.core.IdeasegAnalyzer;
import com.indexea.ideaseg.core.IdeasegIOAdapter;
import com.indexea.ideaseg.core.TokenizerMode;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 分词的压力测试
 */
public class AnalyzerTester {

    static {
        IdeasegIOAdapter.BASE_PATH = "/Users/javayou/Projects/indexea/ideaseg/";
    }

    private final static IdeasegAnalyzer analyzer = new IdeasegAnalyzer(null, TokenizerMode.speed);

    public static void main(String[] args) throws IOException {
        testPerceptron();
        /*
        long ct = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++)
            System.out.println(segmenter.segment(text));
        System.out.printf("words: %d, time: %d\n", text.length() * 1000, (System.currentTimeMillis() - ct));
        */
    }

    public static void testPerceptron() throws IOException {
        String text = "Hello J2Cache 上海华安工业（集团）公司董事长谭旭光和秘书胡花蕊来到美国纽约现代艺术博物馆参观, 香港特别行政区的张朝阳说商品和服务是三原县鲁桥食品厂的主营业务";
        PerceptronLexicalAnalyzer analyzer = new PerceptronLexicalAnalyzer(
                HanLP.Config.PerceptronCWSModelPath,
                HanLP.Config.PerceptronPOSModelPath,
                HanLP.Config.PerceptronNERModelPath);
        PerceptronSegmenter segmenter = new PerceptronSegmenter(HanLP.Config.PerceptronCWSModelPath);
        CRFSegmenter crf = new CRFSegmenter("data/model/crf/pku199801/cws.txt.bin");
        System.out.println("感知分词(老): " + analyzer.seg(text));
        System.out.println("感知分词: " + segmenter.segment(text));
        System.out.println("CRF分词: " + crf.segment(text));
        System.out.println("HanLP: " + HanLP.segment(text));
    }

    private static void singleTest() {
        HanLP.Config.DEBUG = true;
        String text = "Hello 江西鄱阳湖干枯，中国最大淡水湖变成大草原";
        System.out.println(SpeedTokenizer.segment(text));
        //analyzer.getSegment().seg(text).stream().forEach(System.out::println);
        //tokens(text).stream().forEach(System.out::println);
    }

    private static List<String> tokens(String text) {
        List<String> tokens = new ArrayList<>();
        try (TokenStream stream = analyzer.tokenStream(null, text)) {
            stream.reset();
            CharTermAttribute offsetAtt = stream.addAttribute(CharTermAttribute.class);
            while (stream.incrementToken()) {
                tokens.add(offsetAtt.toString());
            }
            stream.end();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return tokens;
    }
}
