package com.indexea.ideaseg.elasticsearch;

import com.indexea.ideaseg.core.IdeasegAnalyzer;
import com.indexea.ideaseg.core.TokenizerMode;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.env.Environment;
import org.elasticsearch.index.IndexSettings;
import org.elasticsearch.index.analysis.AbstractIndexAnalyzerProvider;

public class IdeasegAnalyzerProvider extends AbstractIndexAnalyzerProvider<IdeasegAnalyzer> {

    private final IdeasegAnalyzer analyzer;

    private IdeasegAnalyzerProvider(IndexSettings indexSettings, Environment env, String name, Settings settings, TokenizerMode mode) {
        super(indexSettings, name, settings);
        this.analyzer = new IdeasegAnalyzer(indexSettings.getIndex().getName(), mode);
    }

    public static IdeasegAnalyzerProvider standard(IndexSettings indexSettings, Environment env, String name, Settings settings) {
        return new IdeasegAnalyzerProvider(indexSettings, env, name, settings, TokenizerMode.standard);
    }

    public static IdeasegAnalyzerProvider speed(IndexSettings indexSettings, Environment env, String name, Settings settings) {
        return new IdeasegAnalyzerProvider(indexSettings, env, name, settings, TokenizerMode.speed);
    }

    @Override
    public IdeasegAnalyzer get() {
        return analyzer;
    }
}
