package com.indexea.ideaseg.elasticsearch;

import com.indexea.ideaseg.core.IdeasegTokenizer;
import com.indexea.ideaseg.core.TokenizerMode;
import org.apache.lucene.analysis.Tokenizer;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.env.Environment;
import org.elasticsearch.index.IndexSettings;
import org.elasticsearch.index.analysis.AbstractTokenizerFactory;

/**
 * Ideaseg Tokenizer
 */
public class IdeasegTokenizerFactory extends AbstractTokenizerFactory {

    private String indexName;
    private TokenizerMode mode;

    private IdeasegTokenizerFactory(IndexSettings isettings, Environment env, String name, Settings settings, TokenizerMode mode) {
        super(isettings, settings, name);
        this.indexName = isettings.getIndex().getName();
        this.mode = mode;
    }

    /**
     * 标准模式
     *
     * @param isettings
     * @param env
     * @param name
     * @param settings
     * @return
     */
    public static IdeasegTokenizerFactory standard(IndexSettings isettings, Environment env, String name, Settings settings) {
        return new IdeasegTokenizerFactory(isettings, env, name, settings, TokenizerMode.standard);
    }

    /**
     * 极速模式
     *
     * @param isettings
     * @param env
     * @param name
     * @param settings
     * @return
     */
    public static IdeasegTokenizerFactory speed(IndexSettings isettings, Environment env, String name, Settings settings) {
        return new IdeasegTokenizerFactory(isettings, env, name, settings, TokenizerMode.speed);
    }

    @Override
    public Tokenizer create() {
        return new IdeasegTokenizer(indexName, mode);
    }
}
