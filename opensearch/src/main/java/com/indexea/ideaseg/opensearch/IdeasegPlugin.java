package com.indexea.ideaseg.opensearch;

import com.hankcs.hanlp.utility.Predefine;
import com.indexea.ideaseg.core.IdeasegIOAdapter;
import com.indexea.ideaseg.core.PluginBase;
import org.apache.lucene.analysis.Analyzer;
import org.opensearch.common.io.PathUtils;
import org.opensearch.common.settings.Settings;
import org.opensearch.env.Environment;
import org.opensearch.index.analysis.AnalyzerProvider;
import org.opensearch.index.analysis.TokenizerFactory;
import org.opensearch.indices.analysis.AnalysisModule.AnalysisProvider;
import org.opensearch.plugins.AnalysisPlugin;
import org.opensearch.plugins.Plugin;

import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 * Ideaseg 的 ElasticSearch 插件
 */
public class IdeasegPlugin extends Plugin implements AnalysisPlugin, PluginBase {

    public final static String PLUGIN_NAME = "ideaseg";

    /**
     * Hanlp配置文件名
     */
    private static final String CONFIG_FILE_NAME = "data" + File.separator + "hanlp.properties";

    public IdeasegPlugin(Settings settings) {
        String home = null;
        if (Environment.PATH_HOME_SETTING.exists(settings))
            home = Environment.PATH_HOME_SETTING.get(settings);
        if (home == null)
            throw new IllegalStateException(Environment.PATH_HOME_SETTING.getKey() + " is not configured");
        Path pluginHome = PathUtils.get(home, "plugins", PLUGIN_NAME);
        IdeasegIOAdapter.BASE_PATH = pluginHome.toString() + File.separator; //设置 HanLP 根目录
        Predefine.HANLP_PROPERTIES_PATH = IdeasegIOAdapter.BASE_PATH + CONFIG_FILE_NAME;
    }

    @Override
    public Map<String, AnalysisProvider<TokenizerFactory>> getTokenizers() {
        Map<String, AnalysisProvider<TokenizerFactory>> extra = new HashMap<>();
        extra.put(MODE_STANDARD, IdeasegTokenizerFactory::standard);
        //extra.put(MODE_SPEED, IdeasegTokenizerFactory::speed);
        return extra;
    }

    @Override
    public Map<String, AnalysisProvider<AnalyzerProvider<? extends Analyzer>>> getAnalyzers() {
        Map<String, AnalysisProvider<AnalyzerProvider<? extends Analyzer>>> analyzers = new HashMap<>();
        analyzers.put(MODE_STANDARD, IdeasegAnalyzerProvider::standard);
        //analyzers.put(MODE_SPEED, IdeasegAnalyzerProvider::speed);
        return analyzers;
    }
}
