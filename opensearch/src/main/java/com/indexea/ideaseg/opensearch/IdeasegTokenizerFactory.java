package com.indexea.ideaseg.opensearch;

import com.indexea.ideaseg.core.IdeasegTokenizer;
import com.indexea.ideaseg.core.TokenizerMode;
import org.apache.lucene.analysis.Tokenizer;
import org.opensearch.common.settings.Settings;
import org.opensearch.env.Environment;
import org.opensearch.index.IndexSettings;
import org.opensearch.index.analysis.AbstractTokenizerFactory;

/**
 * Ideaseg Tokenizer
 */
public class IdeasegTokenizerFactory extends AbstractTokenizerFactory {

    private String indexName;
    private TokenizerMode mode;

    private IdeasegTokenizerFactory(IndexSettings isettings, Environment env, String name, Settings settings, TokenizerMode mode) {
        super(isettings, settings, name);
        this.indexName = isettings.getIndex().getName();
        this.mode = mode;
    }

    /**
     * 标准模式
     *
     * @param isettings
     * @param env
     * @param name
     * @param settings
     * @return
     */
    public static IdeasegTokenizerFactory standard(IndexSettings isettings, Environment env, String name, Settings settings) {
        return new IdeasegTokenizerFactory(isettings, env, name, settings, TokenizerMode.standard);
    }

    /**
     * 极速模式
     *
     * @param isettings
     * @param env
     * @param name
     * @param settings
     * @return
     */
    public static IdeasegTokenizerFactory speed(IndexSettings isettings, Environment env, String name, Settings settings) {
        return new IdeasegTokenizerFactory(isettings, env, name, settings, TokenizerMode.speed);
    }

    @Override
    public Tokenizer create() {
        return new IdeasegTokenizer(indexName, mode);
    }
}
