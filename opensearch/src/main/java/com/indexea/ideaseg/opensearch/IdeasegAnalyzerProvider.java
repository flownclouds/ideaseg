package com.indexea.ideaseg.opensearch;

import com.indexea.ideaseg.core.IdeasegAnalyzer;
import com.indexea.ideaseg.core.TokenizerMode;
import org.opensearch.common.settings.Settings;
import org.opensearch.env.Environment;
import org.opensearch.index.IndexSettings;
import org.opensearch.index.analysis.AbstractIndexAnalyzerProvider;

public class IdeasegAnalyzerProvider extends AbstractIndexAnalyzerProvider<IdeasegAnalyzer> {

    private final IdeasegAnalyzer analyzer;

    private IdeasegAnalyzerProvider(IndexSettings indexSettings, Environment env, String name, Settings settings, TokenizerMode mode) {
        super(indexSettings, name, settings);
        this.analyzer = new IdeasegAnalyzer(indexSettings.getIndex().getName(), mode);
    }

    public static IdeasegAnalyzerProvider standard(IndexSettings indexSettings, Environment env, String name, Settings settings) {
        return new IdeasegAnalyzerProvider(indexSettings, env, name, settings, TokenizerMode.standard);
    }

    public static IdeasegAnalyzerProvider speed(IndexSettings indexSettings, Environment env, String name, Settings settings) {
        return new IdeasegAnalyzerProvider(indexSettings, env, name, settings, TokenizerMode.speed);
    }

    @Override
    public IdeasegAnalyzer get() {
        return analyzer;
    }
}
